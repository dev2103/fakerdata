const faker = require("faker");
const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const _ = require("lodash");
// Connection URL
const url = "mongodb://localhost:27017";

// Database Name
const dbName = "peer";

// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);

    const db = client.db(dbName);

    // get access to the relevant collections
    const usersCollection = db.collection("users");
    const feedbackCollection = db.collection("feedback");
    // make a bunch of users
    let users = [];

    let gallery = [
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR_wXw9CKrO5pk7aOW2iFeYV-OAFkiZ7ncXRQ&usqp=CAU",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQanHwE10dxM2geq1GsRKBe-wPM0LPJXe8hiQ&usqp=CAU",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTqZ8eSWq7_KJYCXbmmlJXl6PN3lXyxJ_JAug&usqp=CAU",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlMcrdUOrFvhhUlDzmx-5O6HRMDQj_9hz4SQ&usqp=CAU",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQhY_pHTabZpaUQI-8Zs9vW-qvGsL4lsElH7g&usqp=CAU",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSKHsF7Sy5ktFq8anVJKZG2r1FAhkDksPddvw&usqp=CAU",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRsSf0VDtVz5fafYhtdK7bXa6OCnRft-qSCYQ&usqp=CAU",
    ];

    let videos = [
        "https://www.youtube.com/watch?v=-77UEct0cZM",
        "https://www.youtube.com/watch?v=-77UEct0cZM",
        "https://www.youtube.com/watch?v=AK-9OWrKrxo",
        "https://www.youtube.com/watch?v=xQwdGV3ne4U",
        "https://www.youtube.com/watch?v=s4vAOYI4QoI",
    ];

    let phones = [
        "212624019136",
        "212618734400",
        "212771747677",
    ];
    let genders= [
        "M",
        "F"
    ];

    for (let i = 0; i < 5; i += 1) {

        const shuffled = gallery.sort(() => 0.5 - Math.random());
        const shuffledvideos = videos.sort(() => 0.5 - Math.random());
        let selected_gallery = shuffled.slice(0, 7);
        let selected_videos = shuffledvideos.slice(0, 5);


        const full_name = faker.name.findName();

        const gender = genders[Math.floor(Math.random() * genders.length)];

        var phone_number = faker.phone.phoneNumber(); // Kassandra.Haley@erich.biz
        const avatar = faker.image.avatar();
        const fcm_token =  faker.random.uuid();

        console.log(full_name + ' ' + gender + ' ' + phone_number +  ' '+ fcm_token +' '+ avatar);

        let newUser = {
            full_name: full_name,
            gender: gender,
            phone_number: phone_number,
            avatar: avatar,
            fcm_token: fcm_token,
            gallery: selected_gallery,
            videos: selected_videos,
        };
        users.push(newUser);

        // visual feedback always feels nice!
        console.log(newUser.email);
    }
   let inserted =  usersCollection.insertMany(users);

    console.log(inserted);
    //
    // // make a bunch of posts
    let feedback = [];
    for (let i = 0; i < 5; i += 1) {

        // const fcm_token =  faker.random.uuid();

        let newFeed = {


            // respectful : 12,
            // loyal : 4
            // who made the feedback user_id : 3
            // made for feedback_to : 5

              respectful: faker.random.number({
                  'min': 10,
                  'max': 50
              }),
                loyal: faker.random.number({
                    'min': 10,
                    'max': 50
                }),
            honest: faker.random.number({
                'min': 10,
                'max': 50
            }),
            humble: faker.random.number({
                'min': 10,
                'max': 50
            }),
            kind: faker.random.number({
                'min': 10,
                'max': 50
            }),
            from: '5f0ed8a31766a94dc86ad02f',
            to:  '5f0ed8c11766a94dc86ad030',
        };

        feedback.push(newFeed);

        // visual feedback again!
        console.log(newFeed.title);
    }

    feedbackCollection.insertMany(feedback);

    console.log("Database seeded! :)");
    client.close();
});
