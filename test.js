const compression = require('compression');
const express = require('express');

const app = express();

// compress all responses

app.use(compression({ filter: shouldCompress }));

function shouldCompress (req, res) {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false
    }

    // fallback to standard filter function
    return compression.filter(req, res)
}

app.get('/', (req, res) => {
    const animal = 'alligator';
    // Send a text/html file back with the word 'alligator' repeated 1000 times
    res.send(animal.repeat(1000));
});

app.listen(4000, '0.0.0.0');
console.log(`Application is running on: `);


